/**
 * Created by Asus on 4/23/2018.
 */
public class FactoryPizza {
    public static IPizza make(String type){
        IPizza pizza;
        switch (type){
            case "carne":
                pizza=new Carne();
                break;
            case "Hawaina":
                pizza=new Hawaina();
                break;
            case "Tradicional":
                pizza=new Tradicional();
                break;
            default:
                pizza=new Tradicional();
                break;
        }
        return pizza;
    }
}
