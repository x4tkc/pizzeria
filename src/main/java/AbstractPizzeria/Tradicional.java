package AbstractPizzeria;

/**
 * Created by Asus on 4/23/2018.
 */
public class Tradicional extends IPizza {
    @Override
    public void cocinar() {
        System.out.println("Cocinando jamon");
    }
    @Override
    public void preparar() {
        System.out.println("Preparando jamon");
    }
}
