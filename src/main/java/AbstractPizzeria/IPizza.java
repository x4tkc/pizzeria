package AbstractPizzeria;

/**
 * Created by Asus on 4/23/2018.
 */
public abstract class IPizza {

    public abstract void cocinar();
    public abstract void preparar();
}
