package AbstractPizzeria;

/**
 * Created by Asus on 4/23/2018.
 */
public class Carne extends IPizza {
    @Override
    public void cocinar() {
        System.out.println("Cocinando carne");
    }
    @Override
    public void preparar() {
        System.out.println("Preparando carne");
    }
}
